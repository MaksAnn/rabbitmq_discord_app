﻿using System;
using ICore;
using System.Drawing;
using System.IO;
using System.Text;

namespace RabbitMQ_Discord
{
    internal partial class Discrod_MQ_client
    {
        public bool Try_send_message_to_discord(Message_headers report)
        {
            if (report.Type == "Message")
                return Send_message(report);

            if (report.Type == "Img")
                return Report_screen(report);

            Logger.Log($"[{DateTimeOffset.UtcNow.DateTime}] {report.UserName} Cant define type of message! WTF? ", false, Color.Red);
            return false;
        }

        private bool Send_message(Message_headers report)
        {
            var decodedBody = Encoding.UTF8.GetString(report.Body);

            return discordClient.Send_message(report.Report_type, decodedBody, report.Log);
        }

        private bool Report_screen(Message_headers report)
        {
            Bitmap bmp;

            using (var ms = new MemoryStream(report.Body))
            {
                bmp = new Bitmap(ms);
                ms.Close();
            }

            return discordClient.Report_screen(report.Report_type, bmp, report.Img_message, report.Log);
        }

    }
}