﻿using ICore;
using RabbitMQ.Client.Events;
using System;
using System.Drawing;
using System.Threading;

namespace RabbitMQ_Discord
{
    internal partial class Discrod_MQ_client
    {
        public override void Consumer_received(object sender, BasicDeliverEventArgs e)
        {
            new Thread(() =>
            {
                lock (e)
                {
                    try
                    {
                        if (Check_exception(e))
                        {
                            Try_ack(e.DeliveryTag);
                            return;
                        }

                        Message_headers headers = new Message_headers(e);

                        if (discordClient == null)
                        {
                            Logger.Log(
                                $"[{DateTimeOffset.UtcNow.DateTime}] {headers.UserName} failed to send {headers.Report_type} to {headers.Server_id}. Reason: Discord Client not initialized",
                                false, Color.Red);
                            Try_ack(e.DeliveryTag);
                            return;
                        }

                        if (!discordClient.Initialize(headers.Server_id, headers.UserName, headers.Email, headers.Alias,
                            headers.Report_type))
                        {
                            Try_ack(e.DeliveryTag);
                            return;
                        }

                        if (Try_send_message_to_discord(headers))
                            Try_ack(e.DeliveryTag);
                    }
                    catch (Exception exception)
                    {
                        Logger.Log($"[{DateTimeOffset.UtcNow.DateTime}] Exception: {exception.Message}", false,
                            Color.Red);
                    }
                }
            }).Start();
        }
    }
}