﻿using System;
using ICore;
using RabbitMQ.Client.Events;
using System.Drawing;

namespace RabbitMQ_Discord
{
    internal partial class Discrod_MQ_client
    {
        public static bool Check_exception(BasicDeliverEventArgs args)
        {
            if (args.Body.IsEmpty)
            {
                Logger.Log($"[{DateTimeOffset.UtcNow.DateTime}] Image body is empty. {args.BasicProperties.Headers["Message"]}, failed to send {args.BasicProperties.Headers["Report_type"]}", false, Color.Red);
                return true;
            }


            //если задача требует обратного ответа  - то в ReplyTo - будет имя очереди отправителя (туда нужно ответить)
            if (!args.BasicProperties.IsReplyToPresent())
            {
                Logger.Log("No sender to reply defined. Ignoring! WTF? ", false, Color.Red);
                return true;
            }

            return false;
        }
    }
}