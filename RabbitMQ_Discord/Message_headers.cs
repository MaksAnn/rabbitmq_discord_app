﻿using ICore;
using RabbitMQ.Client.Events;
using System;
using System.Drawing;
using System.Text;

namespace RabbitMQ_Discord
{
    internal class Message_headers
    {
        //public BasicDeliverEventArgs args;

        public byte[] Body;

        public string Type;

        //Hearers
        public string UserName;

        public string Email;
        public string Alias;
        public string Server_id;
        public string Report_type;
        public string Img_message;
        public string Log;

        public Message_headers(BasicDeliverEventArgs e)
        {
            Body = new byte[e.Body.Length];
            e.Body.CopyTo(this.Body);

            Type = e.BasicProperties.Type;

            UserName = Encoding.UTF8.GetString((byte[])e.BasicProperties.Headers["UserName"]);

            Email = Encoding.UTF8.GetString((byte[])e.BasicProperties.Headers["Email"]);

            Alias = Encoding.UTF8.GetString((byte[])e.BasicProperties.Headers["Alias"]);

            Server_id = Encoding.UTF8.GetString((byte[])e.BasicProperties.Headers["Server_id"]);

            Report_type = Encoding.UTF8.GetString((byte[])e.BasicProperties.Headers["Report_type"]);

            Img_message = Encoding.UTF8.GetString((byte[])e.BasicProperties.Headers["Message"]);
            try
            {
                Log = Encoding.UTF8.GetString((byte[])e.BasicProperties.Headers["Log"]);
            }
            catch (Exception ex)
            {
                Logger.Log($"[{DateTimeOffset.UtcNow.DateTime}] {UserName} Log is absent. Update client", color: Color.Green);
                Log = null;
            }
        }
    }
}