﻿using Discord;
using Discord.WebSocket;
using ICore;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Color = System.Drawing.Color;
using Image = System.Drawing.Image;

namespace RabbitMQ_Discord
{
    public class DiscordClient
    {
        private DiscordSocketClient _client;
        private string Alias;
        private SocketGuild Current_guild;
        private string Email;
        private string Prefix;
        private string Server_ID;
        private string UserName;

        private async Task<bool> WaitForConnected(
            DiscordSocketClient client,
            CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                if (client.ConnectionState == ConnectionState.Connected)
                    return true;
                await Task.Delay(50, token);
            }

            return false;
        }

        public bool Initialize(
            string server_id,
            string userName,
            string email,
            string alias,
            string report_type)
        {
            UserName = userName;
            Alias = alias;
            Server_ID = server_id;
            Email = email;
            Prefix = UserName + " | " + Email + " |";
            if (Server_ID.Length < 12)
            {
                Logger.Log($"[{DateTimeOffset.UtcNow.DateTime}] {Prefix} Failed to send {report_type} to {server_id}. Reason: Discord ID is not correct", color: Color.Red);
                return false;
            }

            if (_client == null)
            {
                Logger.Log($"[{DateTimeOffset.UtcNow.DateTime}] {Prefix} Failed to send {report_type} to {server_id}. Reason: Discord client is null", color: Color.Red);

                return false;
            }

            var guilds = _client.Guilds;
            var server_id_ulong = ulong.Parse(server_id);
            Current_guild = guilds.FirstOrDefault(u => (long)u.Id == (long)server_id_ulong);
            if (Current_guild == null)
            {
                Logger.Log(
                    string.Format("[{0}] {1} Failed to send {2} to {3}. Reason: Add BOT to the server",
                        DateTimeOffset.UtcNow.DateTime, Prefix, report_type, server_id), color: Color.Red);
                return false;
            }

            if (Current_guild.IsSynced)
                return true;
            Logger.Log(
                string.Format("[[{0}] {1}  failed to send {2} to {3}. Reason: Failed to connect {4} ",
                    DateTimeOffset.UtcNow.DateTime, Prefix, report_type, server_id, Current_guild.Name),
                color: Color.Red);
            return false;
        }

        public bool Login_bot(string BotToken)
        {
            _client = new DiscordSocketClient();
            if (!Login(_client, BotToken).GetAwaiter().GetResult())
            {
                Logger.Log("Failed to connect to the discord bot. WTF? Contact the admin. Sleeping 30s",
                    color: Color.Red);
                Thread.Sleep(10000);
                return false;
            }

            _client.SetGameAsync("FIFA 22");
            return true;
        }

        private async Task<bool> Login(DiscordSocketClient client, string bot_token)
        {
            var result = false;
            var cts = new CancellationTokenSource(TimeSpan.FromSeconds(10.0));
            try
            {
                await client.LoginAsync(TokenType.Bot, bot_token);
                await client.StartAsync();
                result = await WaitForConnected(client, cts.Token);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Logger.Log("Exception " + ex.Message);
                Console.ResetColor();
            }

            var flag = result;
            cts = null;
            return flag;
        }

        public bool Report_screen(string Report_type, Bitmap screen, string message, string log)
        {
            var channels = Current_guild.Channels;
            var channel =
                (channels != null
                    ? channels.FirstOrDefault(u =>
                        u.Name.Equals(Report_type, StringComparison.InvariantCultureIgnoreCase))
                    : (IChannel)null) ?? Current_guild.CreateTextChannelAsync(Report_type).GetAwaiter().GetResult();
            if (!Try_send_img((IMessageChannel)channel, screen, message))
            {
                Logger.Log(
                    string.Format("[{0}] {1} Failed to send {2} to {3}. Reason: Unable to send img",
                        DateTimeOffset.UtcNow.DateTime, Prefix, Report_type, Server_ID), false, Color.Red);
                return false;
            }

            if (log == null || log == "")
            {
                Logger.Log(
                    string.Format("[{0}] {1} Send screen success {2}", DateTimeOffset.UtcNow.DateTime, Prefix,
                        Report_type), false, Color.Green);
                return true;
            }

            if (!Try_send_txt((IMessageChannel)channel, log))
            {
                Logger.Log(
                    string.Format("[{0}] {1} Failed to send {2} to {3}. Reason: Unable to send txt",
                        DateTimeOffset.UtcNow.DateTime, Prefix, Report_type, Server_ID), false, Color.Red);
                return false;
            }

            Logger.Log(
                string.Format("[{0}] {1} Send screen success {2}", DateTimeOffset.UtcNow.DateTime, Prefix, Report_type),
                false, Color.Green);
            return true;
        }

        private bool Try_send_txt(IMessageChannel channel, string log)
        {
            DateTimeOffset utcNow;
            for (var index = 0; index < 3; ++index)
                try
                {
                    using (var memoryStream1 = new MemoryStream())
                    {
                        TextWriter textWriter = new StreamWriter(memoryStream1);
                        textWriter.Write(log);
                        textWriter.Flush();
                        memoryStream1.Position = 0L;
                        var messageChannel = channel;
                        var memoryStream2 = memoryStream1;
                        utcNow = DateTimeOffset.UtcNow;
                        var filename = string.Format("{0}.txt", utcNow.ToUnixTimeMilliseconds());
                        messageChannel.SendFileAsync(memoryStream2, filename, "").GetAwaiter().GetResult();
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    utcNow = DateTimeOffset.UtcNow;
                    Logger.Log(
                        string.Format("[{0}] {1} ", utcNow.DateTime, Prefix) + string.Format(
                            "Discord was unable to send txt to {0}. Retrying in 3 seconds.\nException : {1}", Server_ID,
                            ex), color: Color.Red);
                    Thread.Sleep(3000);
                }

            return false;
        }

        private bool Try_send_img(IMessageChannel channel, Bitmap screen, string message)
        {
            DateTimeOffset utcNow;
            for (var index = 0; index < 3; ++index)
                try
                {
                    using (var memoryStream1 = new MemoryStream())
                    {
                        new Bitmap((Image)screen).Save(memoryStream1, System.Drawing.Imaging.ImageFormat.Png);
                        memoryStream1.Seek(0L, SeekOrigin.Begin);
                        var messageChannel = channel;
                        var memoryStream2 = memoryStream1;
                        utcNow = DateTimeOffset.UtcNow;
                        var filename = string.Format("{0}.png", utcNow.ToUnixTimeMilliseconds());
                        var text = "|" + Alias + "| " + Email + "| \n" + message;
                        messageChannel.SendFileAsync(memoryStream2, filename, text).GetAwaiter().GetResult();
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    utcNow = DateTimeOffset.UtcNow;
                    Logger.Log(
                        string.Format("[{0}] {1} ", utcNow.DateTime, Prefix) + string.Format(
                            "Discord was unable to send img to {0}. Retrying in 3 seconds. \nException : {1}",
                            Server_ID, ex), color: Color.Red);
                    Thread.Sleep(3000);
                }

            return false;
        }

        public bool Send_message(string Report_type, string message, string log)
        {
            var channel =
                (IChannel)Current_guild.Channels.FirstOrDefault(u =>
                   u.Name.Equals(Report_type, StringComparison.InvariantCultureIgnoreCase)) ??
                Current_guild.CreateTextChannelAsync(Report_type).GetAwaiter().GetResult();
            if (log == null || log == "")
            {
                if (!Try_send_message((IMessageChannel)channel, message))
                {
                    Logger.Log(
                        string.Format("[{0}] {1} Failed to send {2} to {3}. Reason: Unable to send msg",
                            DateTimeOffset.UtcNow.DateTime, Prefix, Report_type, Server_ID), false, Color.Red);
                    return false;
                }
            }
            else if (!Try_send_txt_and_message((IMessageChannel)channel, log, message))
            {
                Logger.Log(
                    string.Format("[{0}] {1} Failed to send to {2} to {3}. Reason: Unable to send txt msg",
                        DateTimeOffset.UtcNow.DateTime, Prefix, Report_type, Server_ID), false, Color.Red);
                return false;
            }

            Logger.Log(
                string.Format("[{0}] {1} Send screen success {2}", DateTimeOffset.UtcNow.DateTime, Prefix, Report_type),
                false, Color.Green);
            return true;
        }

        private bool Try_send_txt_and_message(IMessageChannel channel, string log, string message)
        {
            DateTimeOffset utcNow;
            for (var index = 0; index < 3; ++index)
                try
                {
                    using (var memoryStream1 = new MemoryStream())
                    {
                        TextWriter textWriter = new StreamWriter(memoryStream1);
                        textWriter.Write(log);
                        textWriter.Flush();
                        memoryStream1.Position = 0L;
                        var messageChannel = channel;
                        var memoryStream2 = memoryStream1;
                        utcNow = DateTimeOffset.UtcNow;
                        var filename = string.Format("{0}.txt", utcNow.ToUnixTimeMilliseconds());
                        var text = Alias + " \n " + UserName + " " + Email + "\n" + message;
                        messageChannel.SendFileAsync(memoryStream2, filename, text).GetAwaiter().GetResult();
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    utcNow = DateTimeOffset.UtcNow;
                    Logger.Log(
                        string.Format("[{0}] {1} {2} ", utcNow.DateTime, UserName, Email) + string.Format(
                            "Discord was unable to send txt message to {0}. Retrying in 3 seconds.\nException : {1}",
                            Server_ID, ex), color: Color.Red);
                    Thread.Sleep(3000);
                }

            return false;
        }

        private bool Try_send_message(IMessageChannel channel, string message)
        {
            for (var index = 0; index < 3; ++index)
                try
                {
                    channel.SendMessageAsync("|" + Alias + "| " + Email + "| \n" + message).GetAwaiter().GetResult();
                    return true;
                }
                catch (Exception ex)
                {
                    Logger.Log(
                        string.Format("[{0}] {1} {2} ", DateTimeOffset.UtcNow.DateTime, UserName, Email) +
                        string.Format("Discord was unable to send msg to {0}. Retrying in 3 seconds.\nException : {1}",
                            Server_ID, ex), color: Color.Red);
                    Thread.Sleep(3000);
                }

            return false;
        }
    }
}